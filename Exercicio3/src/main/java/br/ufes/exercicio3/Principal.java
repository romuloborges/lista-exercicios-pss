package br.ufes.exercicio3;

import br.ufes.exercicio3.builder.ComputadorGamerBuilder;
import br.ufes.exercicio3.builder.ComputadorDiretor;

/**
 *
 * @author rborges
 */
public class Principal {
    
    public static void main(String[] args) {
        ComputadorDiretor cd = new ComputadorDiretor();
        
        var computador = cd.construir(new ComputadorGamerBuilder());
        
        System.out.println(computador.getDescricao());
        System.out.println(computador.getPreco());
    }    
    
}
