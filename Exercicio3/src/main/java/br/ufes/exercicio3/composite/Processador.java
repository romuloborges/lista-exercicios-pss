package br.ufes.exercicio3.composite;

/**
 *
 * @author rborges
 */
public class Processador extends Todo {
    
    public Processador(String descricao, double preco) {
        super(descricao, preco);
    }

    @Override
    public double getPreco() {
        double precoTotal = this.preco;
        for(Elemento elemento : elementos) {
            precoTotal += elemento.getPreco();
        }
        return precoTotal;
    }

    @Override
    public String getDescricao() {
        String descricaoCompleta = this.descricao;
        for(Elemento elemento : elementos) {
            descricaoCompleta += ", " + elemento.getDescricao();
        }
        return descricaoCompleta;
    }
    
}
