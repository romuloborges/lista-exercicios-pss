/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufes.exercicio3.composite;

/**
 *
 * @author rborges
 */
public class HD extends Todo {
    
    public HD(String descricao, double preco) {
        super(descricao, preco);
    }

    @Override
    public double getPreco() {
        double precoTotal = this.preco;
        for(Elemento elemento : elementos) {
            precoTotal += elemento.getPreco();
        }
        return precoTotal;
    }

    @Override
    public String getDescricao() {
        String descricaoCompleta = this.descricao;
        for(Elemento elemento : elementos) {
            descricaoCompleta += ", " + elemento.getDescricao();
        }
        return descricaoCompleta;
    }
    
}
