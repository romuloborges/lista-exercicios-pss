package br.ufes.exercicio3.composite;

/**
 *
 * @author rborges
 */
public abstract class Elemento {
    
    protected String descricao;
    protected double preco;
    
    public Elemento(String descricao, double preco) {
        if(descricao == null || descricao.isBlank()) {
            throw new RuntimeException("A descrição fornecida não é válida");
        }
        
        if(preco < 0) {
            throw new RuntimeException("O preço fornecido não é válido");
        }
        
        this.descricao = descricao;
        this.preco = preco;
    }
    
    public abstract double getPreco();
    public abstract String getDescricao();
    
}
