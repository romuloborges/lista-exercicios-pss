package br.ufes.exercicio3.composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rborges
 */
public abstract class Todo extends Elemento {
    
    protected List<Elemento> elementos = new ArrayList<>();
    
    public Todo(String descricao, double preco) {
        super(descricao, preco);
    }
    
    public void add(Elemento elemento) {
        this.elementos.add(elemento);
    }
    
}
