package br.ufes.exercicio3.builder;

import br.ufes.exercicio3.composite.Todo;

/**
 *
 * @author rborges
 */
public abstract class ComputadorBuilder {
    
    private Todo computador;
    
    public abstract void montarComputador();
    
    public final Todo getComputador() {
        return computador;
    }
    
    public void setComputador(Todo computador) {
        this.computador = computador;
    }
    
}
