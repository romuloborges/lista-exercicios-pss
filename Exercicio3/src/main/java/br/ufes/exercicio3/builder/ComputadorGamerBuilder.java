package br.ufes.exercicio3.builder;

import br.ufes.exercicio3.composite.Computador;
import br.ufes.exercicio3.composite.HD;
import br.ufes.exercicio3.composite.MemoriaRAM;
import br.ufes.exercicio3.composite.PlacaVideo;
import br.ufes.exercicio3.composite.Processador;

/**
 *
 * @author rborges
 */
public class ComputadorGamerBuilder extends ComputadorBuilder {

    @Override
    public void montarComputador() {
        this.setComputador(new Computador("Computador gamer", 100));
        this.getComputador().add(new HD("HD Seagate 1TB", 250));
        this.getComputador().add(new MemoriaRAM("HyperX Fury 8GB", 450));
        this.getComputador().add(new MemoriaRAM("HyperX Fury 8GB", 450));
        this.getComputador().add(new PlacaVideo("RTX 2080", 3000));
        this.getComputador().add(new Processador("AMD Ryzen 9 3900x", 2800));
    }
    
}
