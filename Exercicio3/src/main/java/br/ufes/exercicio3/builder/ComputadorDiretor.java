package br.ufes.exercicio3.builder;

import br.ufes.exercicio3.composite.Todo;

/**
 *
 * @author rborges
 */
public class ComputadorDiretor {
    
    public Todo construir(ComputadorBuilder builder) {
        builder.montarComputador();
        
        return builder.getComputador();
    }
    
}
