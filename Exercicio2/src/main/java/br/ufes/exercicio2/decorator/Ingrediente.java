package br.ufes.exercicio2.decorator;

/**
 *
 * @author rborges
 */
public class Ingrediente extends DecoratorSanduiche {

    public Ingrediente(Elemento elemento, double preco, String descricao) {
        super(elemento, preco, descricao);
    }
    
    @Override
    public double getPreco() {
        return getElementoDecorado().getPreco() + preco;
    }

    @Override
    public String getDescricao() {
        return getElementoDecorado().getDescricao() + ", " + descricao;
    }
    
}
