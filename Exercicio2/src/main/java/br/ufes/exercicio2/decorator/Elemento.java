package br.ufes.exercicio2.decorator;

/**
 *
 * @author rborges
 */
public abstract class Elemento {
    
    protected double preco;
    protected String descricao;
    
    public Elemento(double preco, String descricao) {
        if(preco <= 0) {
            throw new RuntimeException("O valor fornecido é inválido");
        }
        if(descricao == null || descricao.isBlank()) {
            throw new RuntimeException("Descrição fornecida é inválida");
        }
        
        this.preco = preco;
        this.descricao = descricao;
    }
    
    public abstract double getPreco();
    public abstract String getDescricao();
    
}
