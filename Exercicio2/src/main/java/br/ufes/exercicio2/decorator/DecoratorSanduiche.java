package br.ufes.exercicio2.decorator;

/**
 *
 * @author rborges
 */
public abstract class DecoratorSanduiche extends Elemento {
    
    private Elemento elementoDecorado;
    
    public DecoratorSanduiche(Elemento elemento, double preco, String descricao) {
        super(preco, descricao);
        if(elemento == null) {
            throw new RuntimeException("Elemento fornecido é inválido");
        }
        this.elementoDecorado = elemento;
    }

    public Elemento getElementoDecorado() {
        return elementoDecorado;
    }
    
}
