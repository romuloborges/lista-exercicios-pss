package br.ufes.exercicio2.decorator;

/**
 *
 * @author rborges
 */
public class Pao extends Elemento {
    
    public Pao(double preco, String descricao) {
        super(preco, descricao);
    }

    @Override
    public double getPreco() {
        return preco;
    }

    @Override
    public String getDescricao() {
        return descricao;
    }
    
}
