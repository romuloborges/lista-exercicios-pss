package br.ufes.exercicio2.diretor;

import br.ufes.exercicio2.decorator.Elemento;
import br.ufes.exercicio2.builder.SanduicheBuilder;

/**
 *
 * @author rborges
 */
public class SanduicheDiretor {
    
    public Elemento construir(SanduicheBuilder builder) {
        builder.colocaPao();
        builder.colocaRecheio();
        
        return builder.getSanduiche();
    }
    
}
