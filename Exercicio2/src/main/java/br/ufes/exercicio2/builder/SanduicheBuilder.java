package br.ufes.exercicio2.builder;

import br.ufes.exercicio2.decorator.Elemento;

/**
 *
 * @author rborges
 */
public abstract class SanduicheBuilder {
    
    private Elemento sanduiche;
    
    public abstract void colocaPao();
    
    public abstract void colocaRecheio();
    
    public final Elemento getSanduiche() {
        return sanduiche;
    }

    public void setSanduiche(Elemento sanduiche) {
        this.sanduiche = sanduiche;
    }
    
}
