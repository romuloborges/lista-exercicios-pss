package br.ufes.exercicio2.builder;

import br.ufes.exercicio2.decorator.Ingrediente;
import br.ufes.exercicio2.decorator.Pao;

/**
 *
 * @author rborges
 */
public class CachorroQuenteBuilder extends SanduicheBuilder {

    @Override
    public void colocaPao() {
        setSanduiche(new Pao(1, "Pão sovado"));
    }

    @Override
    public void colocaRecheio() {
        setSanduiche(new Ingrediente(getSanduiche(), 0.5, "Salsicha"));
    }
    
}
