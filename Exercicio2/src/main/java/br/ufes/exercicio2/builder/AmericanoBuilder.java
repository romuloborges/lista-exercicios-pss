package br.ufes.exercicio2.builder;

import br.ufes.exercicio2.decorator.Ingrediente;
import br.ufes.exercicio2.decorator.Pao;

/**
 *
 * @author rborges
 */
public class AmericanoBuilder extends SanduicheBuilder {

    @Override
    public void colocaPao() {
        setSanduiche(new Pao(0.5, "Pão comum"));
    }

    @Override
    public void colocaRecheio() {
        setSanduiche(new Ingrediente(getSanduiche(), 0.5, "Presunto"));
        setSanduiche(new Ingrediente(getSanduiche(), 0.5, "Queijo"));
        setSanduiche(new Ingrediente(getSanduiche(), 0.8, "Ovo"));
        setSanduiche(new Ingrediente(getSanduiche(), 0.4, "Alface"));
        setSanduiche(new Ingrediente(getSanduiche(), 0.4, "Tomate"));
        setSanduiche(new Ingrediente(getSanduiche(), 0.5, "Maionese"));
    }
    
}
