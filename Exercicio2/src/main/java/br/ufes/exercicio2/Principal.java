package br.ufes.exercicio2;

import br.ufes.exercicio2.decorator.Ingrediente;
import br.ufes.exercicio2.diretor.SanduicheDiretor;
import br.ufes.exercicio2.builder.AmericanoBuilder;
import br.ufes.exercicio2.builder.CachorroQuenteBuilder;

/**
 *
 * @author rborges
 */
public class Principal {
    
    public static void main(String[] args) {
        SanduicheDiretor sd = new SanduicheDiretor();
        
        // Americano
        var sanduiche = sd.construir(new AmericanoBuilder());
        sanduiche = new Ingrediente(sanduiche, 0.9, "Ovo frito");
        
        System.out.println(sanduiche.getDescricao());
        System.out.println(sanduiche.getPreco());
        
        // Cachorro quente
        sanduiche = sd.construir(new CachorroQuenteBuilder());
        sanduiche = new Ingrediente(sanduiche, 0.5, "Ketchup");
        
        System.out.println(sanduiche.getDescricao());
        System.out.println(sanduiche.getPreco());
    }
    
}
