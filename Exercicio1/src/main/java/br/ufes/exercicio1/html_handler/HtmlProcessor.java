package br.ufes.exercicio1.html_handler;

import br.ufes.exercicio1.processor.Processor;

/**
 *
 * @author rborges
 */
public class HtmlProcessor implements Processor {
    
    private HtmlHandler primeiro;
    private HtmlHandler sucessor;
    
    public HtmlProcessor(HtmlHandler handler) {
        if(handler == null) {
            throw new RuntimeException("Handler fornecido é inválido");
        }
        
        primeiro = handler;
        sucessor = handler;
    }
    
    public void addHtmlHandler(HtmlHandler handler) {
        if(handler == null) {
            throw new RuntimeException("Handler fornecido é inválido");
        }
        
        sucessor.setSucessor(handler);
        sucessor = handler;
    }
    
    @Override
    public String handleRequest(String texto) {
        if(texto == null || texto.isBlank()) {
            throw new RuntimeException("Texto inválido");
        }
        
        return primeiro.handleRequest(texto);
    }
    
}
