package br.ufes.exercicio1.html_handler;

/**
 *
 * @author rborges
 */
public class AnchorHandler extends HtmlHandler {
    
    private final String palavra = "<a href";
    
    @Override
    public String handleRequest(String texto) {
        if(texto.toLowerCase().contains(palavra)) {
           return "Mensagem removida por conter conteúdo não autorizado"; 
        } else if(sucessor != null) {
            return sucessor.handleRequest(texto);
        } 
        
        return texto;
    }
    
}
