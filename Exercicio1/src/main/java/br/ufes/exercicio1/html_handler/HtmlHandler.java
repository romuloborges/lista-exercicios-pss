package br.ufes.exercicio1.html_handler;

/**
 *
 * @author rborges
 */
public abstract class HtmlHandler {
    
    protected HtmlHandler sucessor;
    
    public abstract String handleRequest(String texto);
    
    public void setSucessor(HtmlHandler sucessor) {
        this.sucessor = sucessor;
    }
    
}
