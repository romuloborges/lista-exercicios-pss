package br.ufes.exercicio1.participante;

import br.ufes.exercicio1.mediador.MediadorChat;

/**
 *
 * @author rborges
 */
public class ParticipanteChat extends Participante {
    
    private String nome;
    
    public ParticipanteChat(MediadorChat mediadorChat, String nomeParticipante) {
        if(mediadorChat == null) {
            throw new RuntimeException("Mediador do chat fornecido é inválido");
        }
        
        if(nomeParticipante == null || nomeParticipante.isBlank()) {
            throw new RuntimeException("Nome do participante é inválido");
        }
        
        this.nome = nomeParticipante;
        this.mediador = mediadorChat;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public void enviar(String mensagem) {
        this.mediador.enviar(this, mensagem);
    }

    @Override
    public void receber(String mensagem, Participante participante) {
        System.out.println(participante.getNome() + ": " + mensagem);
    }
    
}
