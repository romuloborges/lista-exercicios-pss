package br.ufes.exercicio1.participante;

import br.ufes.exercicio1.mediador.MediadorChat;

/**
 *
 * @author rborges
 */
public abstract class Participante {
    
    protected MediadorChat mediador;
    
    public abstract String getNome();
    public abstract void enviar(String mensagem);
    public abstract void receber(String mensagem, Participante participante);
    
}
