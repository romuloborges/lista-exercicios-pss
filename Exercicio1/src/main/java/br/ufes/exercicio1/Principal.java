package br.ufes.exercicio1;

import br.ufes.exercicio1.builder.MediadorChatDiretor;
import br.ufes.exercicio1.builder.SalaChatHtmlProibidoBuilder;
import br.ufes.exercicio1.builder.SalaChatMarcaProibidaBuilder;
import br.ufes.exercicio1.mediador.MediadorChat;
import br.ufes.exercicio1.participante.Participante;

/**
 *
 * @author rborges
 */
public class Principal {
    
    public static void main(String[] args) {
        MediadorChatDiretor mcd = new MediadorChatDiretor();
        
        MediadorChat mcHtml = mcd.construir(new SalaChatHtmlProibidoBuilder());
        MediadorChat mcMarca = mcd.construir(new SalaChatMarcaProibidaBuilder());
        
        Participante participanteRomulo = mcHtml.criarParticipante(mcHtml, "Rômulo");
        Participante participanteJoao = mcHtml.criarParticipante(mcHtml, "João");
        
        System.out.println("Chat em que tags html são proibidas");
        participanteJoao.enviar("testando minha <table>");
        participanteRomulo.enviar("Não pode tag html nesse chat");
        System.out.println("Fim do chat em que tags html são proibidas\n");
        
        participanteRomulo = mcMarca.criarParticipante(mcMarca, "Rômulo");
        participanteJoao = mcMarca.criarParticipante(mcMarca, "João");
        
        System.out.println("Chat em que marcas são proibidas");
        participanteRomulo.enviar("Computador da Apple, IBM ou o surface da Microsoft?");
        participanteJoao.enviar("Algumas marcas são banidas do chat");
        System.out.println("Fim do chat em que marcas são proibidas");
    }
    
}
