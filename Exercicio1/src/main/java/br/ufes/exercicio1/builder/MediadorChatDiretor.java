package br.ufes.exercicio1.builder;

import br.ufes.exercicio1.mediador.MediadorChat;

/**
 *
 * @author rborges
 */
public class MediadorChatDiretor {
    
    public MediadorChat construir(MediadorChatBuilder builder) {
        builder.montaSalaChat();
        builder.colocaInterceptadorDeMensagem();
        
        return builder.getMediadorChat();
    }
    
}
