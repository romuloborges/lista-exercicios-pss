package br.ufes.exercicio1.builder;

import br.ufes.exercicio1.mediador.MediadorChat;

/**
 *
 * @author rborges
 */
public abstract class MediadorChatBuilder {
    
    private MediadorChat mediadorChat;
    
    public abstract void montaSalaChat();
    public abstract void colocaInterceptadorDeMensagem();

    public final MediadorChat getMediadorChat() {
        return mediadorChat;
    }
    
    public void setMediadorChat(MediadorChat mediadorChat) {
        if(mediadorChat == null) {
            throw new RuntimeException("Mediador de Chat fornecido é inválido");
        }
        
        this.mediadorChat = mediadorChat;
    }
    
}
