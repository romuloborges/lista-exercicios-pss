package br.ufes.exercicio1.builder;

import br.ufes.exercicio1.html_handler.AnchorHandler;
import br.ufes.exercicio1.html_handler.HtmlProcessor;
import br.ufes.exercicio1.html_handler.ImageHandler;
import br.ufes.exercicio1.html_handler.ParagraphHandler;
import br.ufes.exercicio1.html_handler.TableHandler;
import br.ufes.exercicio1.mediador.SalaChat;
import br.ufes.exercicio1.proxy.ProxySalaChat;

/**
 *
 * @author rborges
 */
public class SalaChatHtmlProibidoBuilder extends MediadorChatBuilder {

    @Override
    public void montaSalaChat() {
        setMediadorChat(new ProxySalaChat(new SalaChat()));
    }

    @Override
    public void colocaInterceptadorDeMensagem() {
        HtmlProcessor processor = new HtmlProcessor(new AnchorHandler());
        processor.addHtmlHandler(new ImageHandler());
        processor.addHtmlHandler(new ParagraphHandler());
        processor.addHtmlHandler(new TableHandler());
        
        ((ProxySalaChat)getMediadorChat()).setProcessor(processor);
    }
    
}
