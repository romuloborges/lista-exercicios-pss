package br.ufes.exercicio1.builder;

import br.ufes.exercicio1.marca_handler.AppleHandler;
import br.ufes.exercicio1.marca_handler.IBMHandler;
import br.ufes.exercicio1.marca_handler.MarcaProcessor;
import br.ufes.exercicio1.marca_handler.MicrosoftHandler;
import br.ufes.exercicio1.mediador.SalaChat;
import br.ufes.exercicio1.proxy.ProxySalaChat;

/**
 *
 * @author rborges
 */
public class SalaChatMarcaProibidaBuilder extends MediadorChatBuilder {

    @Override
    public void montaSalaChat() {
        setMediadorChat(new ProxySalaChat(new SalaChat()));
    }

    @Override
    public void colocaInterceptadorDeMensagem() {
        MarcaProcessor processor = new MarcaProcessor(new AppleHandler());
        processor.addMarcaHandler(new IBMHandler());
        processor.addMarcaHandler(new MicrosoftHandler());
        
        ((ProxySalaChat)getMediadorChat()).setProcessor(processor);
    }
    
}
