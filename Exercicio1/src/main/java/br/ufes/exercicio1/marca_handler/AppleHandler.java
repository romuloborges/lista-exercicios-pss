package br.ufes.exercicio1.marca_handler;

import br.ufes.exercicio1.utilitario_string.UtilitarioString;

/**
 *
 * @author rborges
 */
public class AppleHandler extends MarcaHandler {
    
    private final String palavra = "Apple";

    @Override
    public String handleRequest(String texto) {
        texto = UtilitarioString.getInstance().substitui(texto, palavra, "*".repeat(palavra.length()));
        
        if(sucessor != null) {
            texto = sucessor.handleRequest(texto);
        }
        
        return texto;
    }
    
}
