package br.ufes.exercicio1.marca_handler;

import br.ufes.exercicio1.processor.Processor;

/**
 *
 * @author rborges
 */
public class MarcaProcessor implements Processor {
    
    private MarcaHandler primeiro;
    private MarcaHandler sucessor;
    
    public MarcaProcessor(MarcaHandler handler) {
        if(handler == null) {
            throw new RuntimeException("Handler fornecido é inválido");
        }
        
        primeiro = handler;
        sucessor = handler;
    }
    
    public void addMarcaHandler(MarcaHandler handler) {
        if(handler == null) {
            throw new RuntimeException("Handler fornecido é inválido");
        }
        
        sucessor.setSucessor(handler);
        sucessor = handler;
    }
    
    @Override
    public String handleRequest(String texto) {
        if(texto == null || texto.isBlank()) {
            throw new RuntimeException("Texto inválido");
        }
        
        return primeiro.handleRequest(texto);
    }
    
}
