package br.ufes.exercicio1.marca_handler;

/**
 *
 * @author rborges
 */
public abstract class MarcaHandler {
    
    protected MarcaHandler sucessor;
    
    public abstract String handleRequest(String texto);
    
    public void setSucessor(MarcaHandler sucessor) {
        this.sucessor = sucessor;
    }
    
}
