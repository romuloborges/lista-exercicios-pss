package br.ufes.exercicio1.processor;

/**
 *
 * @author rborges
 */
public interface Processor {
    
    public String handleRequest(String texto);
    
}
