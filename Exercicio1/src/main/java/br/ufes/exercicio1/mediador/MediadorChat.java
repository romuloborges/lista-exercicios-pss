package br.ufes.exercicio1.mediador;

import br.ufes.exercicio1.participante.Participante;

/**
 *
 * @author rborges
 */
public interface MediadorChat {

    public void enviar(Participante participante, String mensagem);
    public Participante criarParticipante(MediadorChat mediador, String nome);
    
}
