package br.ufes.exercicio1.mediador;

import br.ufes.exercicio1.participante.Participante;
import br.ufes.exercicio1.participante.ParticipanteChat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rborges
 */
public class SalaChat implements MediadorChat {
    
    private List<Participante> participantes;
    
    public SalaChat() {
        this.participantes = new ArrayList<>();
    }

    @Override
    public void enviar(Participante participante, String mensagem) {
        if(participante == null) {
            throw new RuntimeException("Participante informado para envio de mensagem é inválido");
        }
        
        if(mensagem == null || mensagem.isBlank()) {
            throw new RuntimeException("Mensagem inválida para envio");
        }
        
        for(var participanteDaSala : participantes) {
            if(!participanteDaSala.equals(participante)) {
                participanteDaSala.receber(mensagem, participante);
            }
        }
    }

    @Override
    public Participante criarParticipante(MediadorChat mediador, String nome) {
        if(nome == null || nome.isBlank()) {
            throw new RuntimeException("Nome do participante é inválido");
        }
        
        Participante participante = new ParticipanteChat(mediador, nome);
        
        this.participantes.add(participante);
        
        return participante;
    }
    
}
