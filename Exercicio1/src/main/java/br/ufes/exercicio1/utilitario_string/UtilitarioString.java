package br.ufes.exercicio1.utilitario_string;

import java.util.Arrays;

/**
 *
 * @author rborges
 */
public class UtilitarioString {
    
    private static UtilitarioString instance;
    
    private UtilitarioString() {}
    
    public static UtilitarioString getInstance() {
        if (instance == null) {
            instance = new UtilitarioString();
        }
        
        return instance;
    }
    
    public String substitui(String texto, String palavra, CharSequence simbolo) {
        String textoParaBusca = texto.toLowerCase();
        palavra = palavra.toLowerCase();
        
        int indexPalavraNoTexto = 0;
        
        char[] vetorTexto = texto.toCharArray();
        char[] vetorTextoParaBusca = texto.toLowerCase().toCharArray();
        
        while((indexPalavraNoTexto = textoParaBusca.indexOf(palavra, indexPalavraNoTexto)) != -1) {
            for(int i = 0; i < palavra.length(); i++) {
                vetorTexto[indexPalavraNoTexto + i] = simbolo.charAt(i);
                vetorTextoParaBusca[indexPalavraNoTexto + i] = simbolo.charAt(i);
            }
            
            textoParaBusca = Arrays.toString(vetorTextoParaBusca);
        }
        
        return new String(vetorTexto);
    }
    
}