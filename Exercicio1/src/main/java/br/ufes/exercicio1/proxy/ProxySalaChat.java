package br.ufes.exercicio1.proxy;

import br.ufes.exercicio1.mediador.MediadorChat;
import br.ufes.exercicio1.participante.Participante;
import br.ufes.exercicio1.processor.Processor;

/**
 *
 * @author rborges
 */
public class ProxySalaChat implements MediadorChat {
    
    private MediadorChat salaChat;
    private Processor processor;
    
    public ProxySalaChat(MediadorChat salaChat) {
        if(salaChat == null) {
            throw new RuntimeException("Sala de chat fornecida é inválida");
        }
        
        this.salaChat = salaChat;
    }

    @Override
    public void enviar(Participante participante, String mensagem) {
        if(processor != null) {
            mensagem = processor.handleRequest(mensagem);
        }
        salaChat.enviar(participante, mensagem);
    }

    @Override
    public Participante criarParticipante(MediadorChat mediador, String nome) {
        return salaChat.criarParticipante(mediador, nome);
    }

    public MediadorChat getSalaChat() {
        return salaChat;
    }

    public void setSalaChat(MediadorChat salaChat) {
        this.salaChat = salaChat;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }
    
}
